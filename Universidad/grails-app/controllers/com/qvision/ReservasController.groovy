package com.qvision
import com.qvision.Libro
import com.qvision.Reservas
import grails.plugin.springsecurity.annotation.Secured
import java.util.Date

@Secured('ROLE_ADMIN')
class ReservasController {

    def estudiante

    def index() { 
    	
    	if (Estudiante.get(params.id) != null) 
            estudiante = Estudiante.get(params.id)
       
        def listaReservas = Reservas.withCriteria {
            estudiante {
                eq('id',estudiante.getId())
            }
        }
        println listaReservas

    	render view:"reserva",model:[lista:listaReservas,estudianteNombre:estudiante]
    }

    def goBack() {
        render view:"menu",model:[estudianteNombre:estudiante]
    }

    def goReservar(){
        def listaLibros = Libro.list()
        render view:"reservar",model:[lista:listaLibros]
     }

     def goLibros(){
     	render view:"libro"
     }

     def guardarReserva() {
        def reserva = new Reservas(estudiante:estudiante.getId(),fecha:params.fecha,libro:params.selectLibros)
        reserva.save(flush:true,failOnError:true)
        redirect view:"index"
     }

     def eliminarReserva() {
        def reserva = Reservas.get(params.id)
        reserva.delete(flush:true,failOnError:true)
        redirect view:"index"
     }
}
