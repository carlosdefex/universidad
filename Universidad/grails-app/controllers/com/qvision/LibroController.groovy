package com.qvision
import com.qvision.Libro
import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class LibroController {

    def index() {
        def listaLibros = Libro.list()
        render view : "libro", model: [lista: listaLibros]
    }

    def editarLibro() {
        def libroId = Libro.get(params.id)
        render view : "editarLibro", model : [libro: libroId]
    }

    def updateLibro() {
        def libroId = Libro.get(params.id)
        libroId.setNombre(params.nombre)
        libroId.save(flush:true,failOnError:true)
        redirect view:"libro"
    }

    def eliminarLibro() {
        def libroId = Libro.get(params.id)
        libroId.delete(flush:true, failOnError:true)
        redirect view:"libro"
    }

    def agregarLibro() {
        render view:"agregarLibro"
    }

    def guardarLibro(){
        def libro = new Libro(nombre:params.nombre)
        libro.save(flush:true,failOnError:true)
        redirect view:"index"
     }
}
