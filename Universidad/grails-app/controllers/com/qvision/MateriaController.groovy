package com.qvision
import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class MateriaController {
    def estudiante

    def index() { 
    	def listaMaterias = Materia.list()
        if (Estudiante.get(params.id) != null) 
           estudiante= Estudiante.get(params.id)
        
    	render view:"index",model:[lista:listaMaterias,estudianteNombre:estudiante]
    }

    def agregarMateria(){
        def listaMaterias = Materia.list()
     	render view:"agregarMateria",model:[lista:listaMaterias,estudianteNombre:estudiante]
     }

     def saveMateria(){
     	def materia = new Materia(nombre:params.nombre,creditos:params.creditos)
     	materia.save(flush:true,failOnError:true)
     	redirect view:"index"
     }

     def deleteMateria(){
     	def materia = Materia.get(params.id)
     	materia.delete(flush:true,failOnError:true)
     	redirect view:"index"
     }

     def editarMateria(){
        def materiaId= Materia.get(params.id)
        render view:"editarMateria",model:[materia:materiaId,estudianteNombre:estudiante]
     }

     def updateMateria(){
           def materiaId= Materia.get(params.id)
           materiaId.setNombre(params.nombre)
           materiaId.setCreditos((params.creditos).toInteger())
           materiaId.save(flush:true,failOnError:true)
           redirect view:"index",model:[estudianteNombre:estudiante]
         }
}
