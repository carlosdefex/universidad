package com.qvision
import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class NotasController {

    def estudiante

    def index() { 
        if (Estudiante.get(params.id) != null) 
           estudiante= Estudiante.get(params.id)
        
        def listaNotas = Notas.withCriteria {
            estudiante {
              eq('id',estudiante.getId())
            }
        }
    	render view:"index",model:[lista:listaNotas,estudianteNombre:estudiante]
    }

    def agregarNota(){
        def listaMaterias = Materia.list()
     	render view:"agregarNota",model:[lista:listaMaterias,estudianteNombre:estudiante]
     }

     def saveNota(){
     	def nota = new Notas(estudiante:estudiante.getId(),materia:params.materiaSel,nota:params.nota)
     	nota.save(flush:true,failOnError:true)
     	redirect view:"index"
     }

     def deleteNota(){
     	def nota = Notas.get(params.id)
     	nota.delete(flush:true,failOnError:true)
     	redirect view:"index"
     }

     def editarNota(){
        def notaId= Notas.get(params.id)
        render view:"editarNota",model:[nota:notaId,estudianteNombre:estudiante]
     }

     def updateNota(){
           def notaId= Notas.get(params.id)
           notaId.setNota((params.notas).toInteger())
           notaId.save(flush:true,failOnError:true)
           redirect view:"index",model:[estudianteNombre:estudiante]
         }
}