package com.qvision
import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class EstudianteController {

	
	def index() {
		def listaEstudiantes= Estudiante.list()
		render view:"index", model:[estudiantes:listaEstudiantes]
	 }

	 def goToChat() {
    	render view:"goToChat"
    }

	 def create(){
	 	render view:"ingresar"
	 }

	 def save(){
	 	def estudianteNuevo = new Estudiante(celular:params.celular,email:params.email,nombre:params.nombre)
    	estudianteNuevo.save(flush:true, failOnError:true)
    	redirect view:"index"
	 }

	 def delete(){
	 	def estudiante=Estudiante.get(params.id)
    	estudiante.delete(flush:true,failOnError:true)
    	redirect  action:'index'
	 }

	 def consultar(){
	 	def estudiante= Estudiante.get(params.id)
	 	render view:"menu",model:[estudianteNombre:estudiante]
	 }

	 def editar(){
	 	def estudiante= Estudiante.get(params.id)
	 	render view:"editar", model:[estudiante:estudiante]
	 }

	 def actualizar(){
	 	def estudiante= Estudiante.get(params.id)
	 	estudiante.setNombre(params.nombre)
	 	estudiante.setCelular(params.celular)
	 	estudiante.setEmail(params.email)
	 	estudiante.save(flush:true,failOnError:true)
	 	redirect view:"index"
	 }
}
