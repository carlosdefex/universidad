<!DOCTYPE html>
<html>
<head>
	<title>Agregar Materia</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
	<div class="container-fluid">
		<div class="col-md-12">
			<g:link action="index" class="btn btn-danger btn-back pull-left" controller="materia" id='${estudianteNombre.id}'>
						Regresar
			</g:link>
		</div>	
		<h2>Agregar Materia</h2>
		<h3>${estudianteNombre.nombre}</h3>	
	</div>

	<div class="container-fluid">
		<g:form name="materiaForm" class="form" action="updateMateria">
		<div class="form-group">
			<input type="text" class="form-control" name="id" placeholder="id" maxlength="20" value="${materia.id}" readonly="">
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="nombre" placeholder="Nombre" maxlength="20" value="${materia.nombre}" required>
		</div>
		<div class="form-group">
			<input type="number" class="form-control" name="creditos" placeholder="Creditos" maxlength="number" min="1" max="10" value="${materia.creditos}" required>
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" name="submit" value="guardar">
		</div>
		</g:form>
	</div>
</body>
</html>