<!DOCTYPE html>
<html>
<head>
	<title>Materias</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
	<div class="container-fluid">
	<div class="col-md-12">
	<g:link action="index" class="btn btn-danger btn-back pull-left" controller="notas" id='${estudianteNombre.id}'>
				Regresar
	</g:link>
	</div>
	<h2>Materias</h2>
	<h3>${estudianteNombre.nombre}</h3>
	        <g:link action="agregarMateria" class="btn btn-primary" controller="materia" id='${estudianteNombre.id}'>
				Agregar Materia
			</g:link>
	</div>
	<div class="container-fluid" id="tablaMateria">
		<div class="row">
			<table class="table table-striped">
				<thead>
					<th>ID</th>
					<th>Nombre</th>
					<th>Creditos</th>
				</thead>
				<tbody>
					<g:each in="${lista}" var="materia">
						<tr>
							<td>${materia.id}</td>
							<td>${materia.nombre}</td>
							<td>${materia.creditos}</td>
							<td>
								<g:link action="editarMateria" class="btn btn-warning" controller="materia" id='${materia.id}'>
									Editar
								</g:link>
								<g:link action="deleteMateria" class="btn btn-danger" controller="materia" id='${materia.id}'>
									Eliminar
								</g:link>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>