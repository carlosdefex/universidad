<!DOCTYPE html>
<html>
<head>
	<title>Libros</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
	<g:link action="index" class="btn btn-danger btn-back" controller="reservas">
		Regresar
	</g:link>
	<hr>
	<div class="container-fluid">
	<h2>Libros</h2>
	<g:link action="agregarLibro" class="btn btn-primary" controller="libro">
		Agregar Libro
	</g:link>
	</div>
	<div class="container-fluid">
		<div class="row">
			<table class="table table-striped">
				<thead>
					<th>ID</th>
					<th>Libro</th>
				</thead>
				<tbody>
					<g:each in="${lista}" var="libro">
						<tr>
							<td>${libro.id}</td>
							<td>${libro.nombre}</td>
							<td>
								<g:link action="editarLibro" class="btn btn-warning" controller="libro" id="${libro.id}">
									Editar
								</g:link>
								<g:link action="eliminarLibro" class="btn btn-danger" controller="libro" id="${libro.id}">
									Eliminar
								</g:link>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>