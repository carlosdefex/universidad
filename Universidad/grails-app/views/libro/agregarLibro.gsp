<!DOCTYPE html>
<html>
<head>
	<title>Agregar Libro</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
	<g:link action="index" class="btn btn-danger btn-back" controller="libro">
		Regresar
	</g:link>
	<hr>
	<div class="container-fluid">
		<h2>Agregar Libro</h2>
	</div>

	<div class="container-fluid">
		<g:form name="libroForm" class="form" action="guardarLibro">
		<div class="form-group">
			<input type="text" class="form-control" name="nombre" placeholder="Nombre" maxlength="100" required>
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" name="submit" value="Guardar">
		</div>
		</g:form>
	</div>
</body>
</html>