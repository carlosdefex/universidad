<!DOCTYPE html>
<html>
<head>
	<title>Ingresar Estudiante</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
<div class="container-fluid">
<h2 id="container-title">Ingresar Estudiante</h2>
</div>

<div id="formulario" class="container-fluid">
	<g:form name="IngresarEstudiante" action="save">
		<div class="form-group">
			<input type="text" name="nombre" placeholder="Nombre" class="form-control" required="">
		</div>
		<div class="form-group">
			<input type="text" name="celular" class="form-control" placeholder="Celular" required="">
		</div>
		<div class="form-group">
			<input type="text" name="email" class="form-control" placeholder="Email" required="">
		</div>
		<div class="form-group">
			<input type="submit" name="submit" class="btn btn-primary" value="Guardar">
		</div>
	</g:form>	
</div>
</body>
</html>