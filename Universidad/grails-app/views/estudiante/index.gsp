<!DOCTYPE html>
<html>
<head>
	<title>Estudiantes</title>
	<asset:stylesheet src="bootstrap.css"/> <!-- Para llamar los estilos css -->
	<asset:stylesheet src="materia.css"/>
</head>
<body>
<div class="container-fluid">
	<h2>Lista De Estudiantes</h2>
</div>
<table class="table table-striped">
	<thead>
		<th>Id</th>
		<th>Nombre</th>
		<th>Celular</th>
		<th>Email</th>	
	</thead>
	<tbody>
	<g:each in="${estudiantes}" var="estu">
		<tr>
			<td>${estu.id}</td>
			<td>${estu.nombre}</td>
			<td>${estu.celular}</td>
			<td>${estu.email}</td>
			<td>
			<g:link action="consultar" controler="estudiante" id="${estu.id}" class="btn btn-success">Consultar</g:link>
			
			
			<g:link action="editar" controler="estudiante" id="${estu.id}" class="btn btn-warning">Editar</g:link>
			
			
			<g:link action="delete" controler="estudiante" id="${estu.id}" class="btn btn-danger">Borrar</g:link>
			</td>
		</tr>
	</g:each>
	</tbody>
</table>
<div class="container-fluid">
<a href="${createLink(controler:'estudiante', action:'create')}" class="btn btn-primary">Crear Estudiante</a>
</div>
</body>
</html>