<!DOCTYPE html>
<html>
<head>
	<title>Menu Principal</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>

	<div class="container-fluid">
		<h2>Bienvenid@,${estudianteNombre.nombre}</h2>	
	</div>
	<div class="container-fluid" id="menu-buttons">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
			<g:link action="index" class="btn btn-primary" controller="notas" id="${estudianteNombre.id}" >
				Consultar Materias
			</g:link>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
			<g:link action="index" class="btn btn-primary" controller="reservas" id="${estudianteNombre.id}">
				Reserva Libros
			</g:link>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
			<g:link action="index" class="btn btn-danger" controller="estudiante">
				Regresar
			</g:link>
			</div>
		</div>
	</div>
</body>
</html>