<!DOCTYPE html>
<html>
<head>
	<title>Editar Estudiante</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>

<body>
	<div class="container-fluid">
		<h2>Editar Estudiantes</h2>
	</div>
	<div class="container-fluid">
	<g:form name="editarEstudiante" class="form" action="actualizar">
	<div class="form-group">
		<input type="text" name="id" value="${estudiante.id}" class="form-control" readonly="" placeholder="id">
	</div>
	<div class="form-group">
		<input type="text" name="nombre" value="${estudiante.nombre}" class="form-control" placeholder="Nombre">
	</div>
	<div class="form-group">
		<input type="text" name="celular" value="${estudiante.celular}" class="form-control" placeholder="Celular">
	</div>
	<div class="form-group">
		<input type="text" name="email" value="${estudiante.email}" class="form-control" placeholder="Email">
	</div>
	<div class="form-group">
			<input type="submit" class="btn btn-primary" name="submit" value="guardar">
	</div>
	</g:form>
	</div>

</body>
</html>