<!DOCTYPE html>
<html>
<head>
	<title>Reservar</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
	<g:link action="index" class="btn btn-danger btn-back" controller="reservas">
		Regresar
	</g:link>
	<hr>
	<div class="container-fluid">
	<h2>Reservar</h2>	
	<div class="container-fluid">
		<div class="row">
			
			<g:form name="reservarForm" class="form" action="guardarReserva">
				<select class="form-control" name="selectLibros">
					<g:each in="${lista}" var="libro">
					    <option value="${libro.id}">${libro.nombre}</option>
					</g:each>
				</select>
				<div class="form-group">
					<g:datePicker class="form-control" name="fecha"/>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" name="submit" value="guardar">
				</div>
			</g:form>
	
		</div>
	</div>
</body>
</html>