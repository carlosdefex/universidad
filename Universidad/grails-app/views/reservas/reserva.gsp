<!DOCTYPE html>
<html>
<head>
	<title>Reservas</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
	<g:link action="consultar" class="btn btn-danger btn-back" controller="estudiante" id='${estudianteNombre.id}'>
		Regresar
	</g:link>
	<hr>
	<div class="container-fluid">
	<h2>Reservas</h2>
	<h3>${estudianteNombre.nombre}</h3>
		<div class="row">			
			<a href="${createLink(controller:'libro',action:'index')}" class="btn btn-primary">Libros</a>
			<a href="${createLink(controller:'reservas',action:'goReservar')}" class="btn btn-primary">Reservar Libro</a>
		</div>	
	</div>
	<div class="container-fluid">
		<div class="row">
			<table class="table table-striped">
				<thead>
					<th>ID</th>
					<th>Estudiante</th>
					<th>Libro</th>
					<th>Fecha</th>				
				</thead>
				<tbody>
					<g:each in="${lista}" var="reserva">
						<tr>
							<td>${reserva.id}</td>
							<td>${estudianteNombre.nombre}</td>
							<td>${reserva.libro.nombre}</td>
							<td>${reserva.fecha}</td>
							<td>				
								<g:link action="eliminarReserva" class="btn btn-danger" controller="reservas" id='${reserva.id}'>
									Eliminar
								</g:link>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>