<!DOCTYPE html>
<html>
<head>
	<title>Agregar Materia</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
	<div class="container-fluid">
		<div class="col-md-12">
		<g:link action="index" class="btn btn-danger btn-back pull-left" controller="notas" id='${estudianteNombre.id}'>
					Regresar
		</g:link>
		</div>
		<h2>Agregar Materia</h2>
		<h3>${estudianteNombre.nombre}</h3>	
	</div>

	<div class="container-fluid">
		<g:form name="motoForm" class="form" action="saveNota">
		<select class="form-group" name="materiaSel">
		<g:each in="${lista}" var="materia">
		    <option value="${materia.id}">${materia.nombre}</option>
		</g:each>
		</select>
		<div class="form-group">
			<input type="number" class="form-control" name="nota" placeholder="Nota" maxlength="number" min="0" max="5" required>
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" name="submit" value="guardar">
		</div>
		</g:form>
	</div>
</body>
</html>