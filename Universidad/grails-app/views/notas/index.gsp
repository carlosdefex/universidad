<!DOCTYPE html>
<html>
<head>
	<title>Materias</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="materia.css"/>
</head>
<body>
	<div class="container-fluid">
	<div class="col-md-12">
	<g:link action="consultar" class="btn btn-danger btn-back pull-left" controller="estudiante" id='${estudianteNombre.id}'>
				Regresar
	</g:link>
	</div>
	<h2>Notas</h2>
	<h3>${estudianteNombre.nombre}</h3>
	        <g:link action="agregarNota" class="btn btn-primary" controller="notas" id='${estudianteNombre.id}'>
				Agregar Nota
			</g:link>
			<g:link action="index" class="btn btn-primary" controller="materia" id='${estudianteNombre.id}'>
				Materias
			</g:link>
	</div>
	<div class="container-fluid" id="tablaMateria">
		<div class="row">
			<table class="table table-striped">
				<thead>
					<th>ID</th>
					<th>Nombre</th>
					<th>Creditos</th>
					<th>Nota</th>
				</thead>
				<tbody>
					<g:each in="${lista}" var="nota">
						<tr>
							<td>${nota.id}</td>
							<td>${nota.materia.nombre}</td>
							<td>${nota.materia.creditos}</td>
							<td>${nota.nota}</td>
							<td>
								<g:link action="editarNota" class="btn btn-warning" controller="notas" id='${nota.id}'>
									Editar
								</g:link>
								<g:link action="deleteNota" class="btn btn-danger" controller="notas" id='${nota.id}'>
									Eliminar
								</g:link>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>