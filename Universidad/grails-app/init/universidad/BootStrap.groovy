package universidad
import com.qvision.Estudiante
import com.qvision.Usuario
import com.qvision.Rol
import com.qvision.UsuarioRol

class BootStrap {


    def init = { servletContext ->
        def usuario, role

        if(Estudiante.count()==0){
    		def estudiante1= new Estudiante(celular: "3113677625",email:"jarango@qvision.com.co",nombre:"Juan Pablo")
    		estudiante1.save(flush:true, failOnError:true)
    		def estudiante2= new Estudiante(celular: "3136166208",email:"srodriguez@qvision.com.co",nombre:"Santiago")
    		estudiante2.save(flush:true, failOnError:true)
			def estudiante3= new Estudiante(celular: "3113236152",email:"cfex@qvision.com.co",nombre:"Carlos Jose")
			estudiante3.save(flush:true, failOnError:true)
    	}
        if(Usuario.count()==0){
            usuario= new Usuario(password:"admin123",username:"admin")
            usuario.save(flush:true,failOnError:true)
        }
        if(Rol.count()==0){
            role= new Rol(authority:"ROLE_ADMIN")
            role.save(flush:true, failOnError:true)
            UsuarioRol.create usuario, role
        }

    }
    
    def destroy = {
    }
}
