package com.qvision
import com.qvision.Notas


class Materia {

	static hasMany = [materia: Notas]

	String nombre
	int creditos

	static constraints = {
		nombre 		blank: false
		creditos 	blank: false
    }

	static mapping = {
		version 	false
	}

}
