package com.qvision

class Notas {

	static belongsTo = [materia: Materia, estudiante: Estudiante]

	int nota

	static mapping = {
		/*id generator: 'increment', name: 'notaId'*/
		version false
	}

    static constraints = {
    
    }
}
