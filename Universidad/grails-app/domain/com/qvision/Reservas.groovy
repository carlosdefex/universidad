package com.qvision
import com.qvision.Estudiante
import com.qvision.Libro

class Reservas {

	static belongsTo = [estudiante: Estudiante, libro: Libro]

	Date fecha

    static constraints = {
    	fecha 	blank: false
    }

    static mapping = {
        /*id generator: 'increment', name: 'reservaId'*/
    	version 	false
    }
}
