package com.qvision
import com.qvision.Reservas

class Libro {

	static hasMany = [libro: Reservas]

	String nombre

    static constraints = {
    	nombre 	blank: false
    }

	static mapping = {
    	version 	false
    }
}
