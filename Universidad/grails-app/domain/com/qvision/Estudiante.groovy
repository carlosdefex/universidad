package com.qvision
import com.qvision.Notas
import com.qvision.Reservas

class Estudiante {

	static hasMany = [estudiante: Notas, estudiante: Reservas]

	String nombre
	String celular
	String email

    static constraints = {
    	nombre     blank: false
    	celular    blank: false
    	email      blank: false, email: true
    }

    static mapping = {
    	version    false
    }
}
