package com.qvision

import grails.gorm.DetachedCriteria
import groovy.transform.ToString

import org.apache.commons.lang.builder.HashCodeBuilder

@ToString(cache=true, includeNames=true, includePackage=false)
class UsuarioRol implements Serializable {

	private static final long serialVersionUID = 1

	Usuario usuario
	Rol rol

	@Override
	boolean equals(other) {
		if (other instanceof UsuarioRol) {
			other.usuarioId == usuario?.id && other.rolId == rol?.id
		}
	}

	@Override
	int hashCode() {
		def builder = new HashCodeBuilder()
		if (usuario) builder.append(usuario.id)
		if (rol) builder.append(rol.id)
		builder.toHashCode()
	}

	static UsuarioRol get(long usuarioId, long rolId) {
		criteriaFor(usuarioId, rolId).get()
	}

	static boolean exists(long usuarioId, long rolId) {
		criteriaFor(usuarioId, rolId).count()
	}

	private static DetachedCriteria criteriaFor(long usuarioId, long rolId) {
		UsuarioRol.where {
			usuario == Usuario.load(usuarioId) &&
			rol == Rol.load(rolId)
		}
	}

	static UsuarioRol create(Usuario usuario, Rol rol) {
		def instance = new UsuarioRol(usuario: usuario, rol: rol)
		instance.save()
		instance
	}

	static boolean remove(Usuario u, Rol r) {
		if (u != null && r != null) {
			UsuarioRol.where { usuario == u && rol == r }.deleteAll()
		}
	}

	static int removeAll(Usuario u) {
		u == null ? 0 : UsuarioRol.where { usuario == u }.deleteAll()
	}

	static int removeAll(Rol r) {
		r == null ? 0 : UsuarioRol.where { rol == r }.deleteAll()
	}

	static constraints = {
		rol validator: { Rol r, UsuarioRol ur ->
			if (ur.usuario?.id) {
				UsuarioRol.withNewSession {
					if (UsuarioRol.exists(ur.usuario.id, r.id)) {
						return ['userRole.exists']
					}
				}
			}
		}
	}

	static mapping = {
		id composite: ['usuario', 'rol']
		version false
	}
}
